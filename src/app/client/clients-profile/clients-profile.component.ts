import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { Client } from '../client.modele';
import { ClientServices } from '../client.service';
@Component({
  selector: 'app-clients-profile',
  templateUrl: './clients-profile.component.html',
  styleUrls: ['./clients-profile.component.css']
})

export class ClientsProfileComponent implements OnInit {
  
  client :Client;
  constructor(private translate : TranslateService,
              private clientService : ClientServices) { }

  ngOnInit() {
     this.client = new Client(this.clientService.currentClient);
  }

  setLanguage(language: string) {
    this.translate.use(language);
    localStorage.setItem('userLanguage',language);
  }

}
