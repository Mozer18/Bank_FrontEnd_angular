import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { apiConfiguration } from '../apiConfiguration';
import { Subject } from 'rxjs/Subject';

import {Client} from './client.modele';
import { Contract } from '../contrat/contrat.modele';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ClientServices{

    //get the selected client for more detail
    clientSelected = new Subject<Client>();
    //current clients
    currentClient : Client; 

    email : string;
    //all clients
    clients : Client[];
    //array of all contracts
    contrats : Contract[]=[];
    //client image
    clientImage : string  = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQF0ZqCD68HDHti64bFIjN3hzeXT-cMLuo3KPGyXRijRAQHddXoNA";

    
    constructor(private http : HttpClient , private apiUrl : apiConfiguration){

    }
 
    getLogIn(email : string, password : string){
     return this.http.post<Client>(this.apiUrl.ServerWithApiUrl+"login",{email: email, password: password}, {responseType: 'json'});    
    }

    getClient(email: string){
        
        return this.http.get<Client>(this.apiUrl.ServerWithApiUrl+"getClient", 
        {
            params : new HttpParams().set("email",email),responseType: 'json'
        });
    }

    getAllClients(): Observable<Client[]>{
        return this.http.get<Client[]>(this.apiUrl.ServerWithApiUrl+"getAllClients", {responseType: 'json'});
    }

    createClient(client : Client){
        this.http.post<Client>(this.apiUrl.ServerWithApiUrl+"createClient",client).subscribe();
    }
}