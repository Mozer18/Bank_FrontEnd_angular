import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientComponent }          from './client.component';
import { ClientsListComponent }     from './clients-list/clients-list.component';
import { ClientsDetailComponent }   from './clients-detail/clients-detail.component';
import { ClientItemComponent }      from './clients-list/client-item/client-item.component';

import { ClientServices }           from './client.service';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatButtonModule, MatInputModule, MatFormFieldModule, MatIconModule, MatOptionModule, MatSelectModule, MatGridListModule} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule }             from '../shared/shared.module';

import { AddClientDialogComponent } from '../shared/add-client-dialog/add-client-dialog.component';
import { ClientsProfileComponent } from './clients-profile/clients-profile.component';

import { AuthGardService as AuthGuard} from '../auth-gard.service';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';

const clientRoutes : Routes=[
  {path: 'clients' , component: ClientComponent, canActivate: [AuthGuard],
  children:[
   {path: ':id' , component: ClientsDetailComponent}
      ]
  },
  {path : 'client-profile' , component :ClientsProfileComponent, canActivate: [AuthGuard]} 
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(clientRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatOptionModule,
    MatSelectModule,
    MatGridListModule,
    BrowserAnimationsModule,
    SharedModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    })
    

  ],
  declarations: [
    ClientComponent,
    ClientsListComponent,
    ClientsDetailComponent,
    ClientItemComponent,
    ClientsProfileComponent
  ],
  entryComponents:[AddClientDialogComponent],
  providers:[
    ClientServices
  ],
  exports:[
    ClientComponent 
  ]

})
export class ClientModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}