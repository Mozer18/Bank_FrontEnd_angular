import { Contract } from "../contrat/contrat.modele";


export class Client{
     public clientId : string;
     public role      : string;
     public firstName : string;
     public lastName  : string;
     public clientImage: string;
     public email       : string;
     public password    : string;
     public contracts  : string[];
     public accounts : string[];
     
     constructor(args: Client){
            this.clientId    = args.clientId;
            this.role        = args.role;
            this.firstName   = args.firstName;
            this.lastName    = args.lastName;
            this.clientImage ="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQF0ZqCD68HDHti64bFIjN3hzeXT-cMLuo3KPGyXRijRAQHddXoNA";
            this.email       = args.email;
            this.password    = args.password;
            if(args.contracts!= null){
                this.contracts = args.contracts;
            }else{
            this.contracts = [];
            }
            this.accounts = [];
     }


}