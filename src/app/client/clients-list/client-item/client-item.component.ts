import { Component, OnInit, Input, OnDestroy} from '@angular/core';
import { Client } from '../../client.modele';
import { ClientServices } from '../../client.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ClientsDetailComponent } from '../../clients-detail/clients-detail.component';
import { AccountService } from '../../../compte/compte.service';
import { Account } from '../../../compte/compte.modele';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-client-item',
  templateUrl: './client-item.component.html',
  styleUrls: ['./client-item.component.css']
})

export class ClientItemComponent implements OnInit, OnDestroy {
   @Input() client :Client;
    clientAccounts : Account[] = [];
    dialogConfig = new MatDialogConfig();

    sub : Subscription;
 

  constructor(private clientService : ClientServices, 
              private accountService : AccountService, 
              private dialog : MatDialog) { }

  ngOnInit() {
      
  }
  


  openDialog(client : Client){

    this.accountService.getClientAccounts(client.clientId).toPromise().then(
      (accounts: Account[])=>{
         this.clientAccounts=accounts; 
        }
    ).then(()=>{ 
          this.dialogConfig.width= "400px";
          this.dialogConfig.data = {
              client  : client,
              contracts : client.contracts,
              accounts : this.clientAccounts
          };    
          
          this.dialog.open(ClientsDetailComponent,this.dialogConfig);
          
    
    });
  }

  ngOnDestroy(){


  }

}
