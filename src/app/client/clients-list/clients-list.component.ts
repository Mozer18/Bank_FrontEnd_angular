import { Component, OnInit, ViewChild, ElementRef, OnDestroy} from '@angular/core';
import { Client } from '../client.modele';
import { ClientServices} from '../client.service';
import { Http } from '@angular/http';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AddClientDialogComponent } from '../../shared/add-client-dialog/add-client-dialog.component';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.css']
})

export class ClientsListComponent implements OnInit, OnDestroy {

  @ViewChild('clientRef') list : ElementRef;

  dialogConfig = new MatDialogConfig();
  clients : Client[]=[];

  currentClient : Client;

  searchByFirstName : string ="";
  searchByLastName  : string ="";
  sub : Subscription[] = [];

  constructor(private clientservices: ClientServices, private http : Http,
              private addDialog : MatDialog ){ 

  }

  ngOnInit() {
    this.sub[0] = this.clientservices.getAllClients().subscribe(
      (clients:Client[])=>{
        this.clients = clients;
      }
    );

    this.sub[1] = this.clientservices.getClient(this.clientservices.email).subscribe(
      (clt)=>{
        this.clientservices.currentClient = clt;
        this.currentClient = clt;
      }
    );

  }

  ngOnDestroy(){
    this.sub.forEach(
      (subscription)=>{
        subscription.unsubscribe();
      }
    );
  }

  onChanges(){
   //console.log(this.list);
    
  }

  openDialog(){
    this.dialogConfig.width= "400px";
    this.addDialog.open(AddClientDialogComponent,this.dialogConfig);
  }

}


