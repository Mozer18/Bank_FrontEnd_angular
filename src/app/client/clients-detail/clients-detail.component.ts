import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { Client } from '../client.modele';
import { ClientServices } from '../client.service';
import { Account } from '../../compte/compte.modele';
import { Contract } from '../../contrat/contrat.modele';
import { ContratServices } from '../../contrat/contrat.service';
import { AccountService } from '../../compte/compte.service';

@Component({
  selector: 'app-clients-detail',
  templateUrl: './clients-detail.component.html',
  styleUrls: ['./clients-detail.component.css']
})
export class ClientsDetailComponent implements OnInit  {

  accounts : Account[];
  contracts : string[]=[];
  client : Client;

  emptyAccount : Account ; 
       
  constructor(private clientService : ClientServices, 
              private contractService : ContratServices,
              private accountService  : AccountService,
              private router : Router,
              private dialogRef : MatDialogRef<ClientsDetailComponent>,
              @Inject(MAT_DIALOG_DATA) public data) { 
    
           }

  ngOnInit() {
    this.client = new Client(this.data.client);
    this.contracts = this.data.contracts;
    this.accounts = this.data.accounts;
    
  }

  onSelectedContract(contractId : string){
    this.contractService.setCurrentContract(contractId).toPromise().then(
      (contract:Contract) => {this.contractService.currentContract = contract;}
    ).then(() => {
      this.onClose();
      this.router.navigate(["/contract-clauses",contractId]);
    });
    
  }

   onSelectedAccount(account : Account){
    this.accountService.setCurrentAccount(account);
    this.onClose();
    this.router.navigate(["/account-operations",account.accountId]);
    
 } 

  onSeeContracts(){
    this.onClose();
    this.router.navigate(["/contracts"]);
    this.clientService.clientSelected.next(this.client);
  }

  onSeeAccounts(){
    this.onClose();
    this.router.navigate(["/comptes"]);
  }

  onClose(){
    this.dialogRef.close('Cancel');
  }

}
