import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ClientServices } from '../../client/client.service';
import { MatSnackBar, MatDialogRef } from '@angular/material';
import { Client } from '../../client/client.modele';
import {TranslateService} from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-client-dialog',
  templateUrl: './add-client-dialog.component.html',
  styleUrls: ['./add-client-dialog.component.css']
})
export class AddClientDialogComponent implements OnInit, OnDestroy {

  sub : Subscription;
  clientForm: FormGroup;
  client : Client;
  roles : String[];

  constructor( private clientService : ClientServices, private snackBar: MatSnackBar,
               private dialogRef : MatDialogRef<AddClientDialogComponent>, 
               private router: Router,
               private translate : TranslateService ) { }

  ngOnInit() {
     this.roles = ["ADMIN", "CLIENT"];

    this.clientForm = new FormGroup({
      'clientId' : new FormControl(null,[Validators.required]),
      'role'     : new FormControl(null,[Validators.required]), 
      'firstName' : new FormControl(null, [Validators.required,Validators.pattern("^[a-zA-Z_-]{2,30}$")]),
      'lastName' : new FormControl(null,[Validators.required,Validators.pattern("^[a-zA-Z_-]{2,30}$")]),
      'email' : new FormControl(null,[Validators.required,Validators.email]),
      'password' : new FormControl(null,[Validators.required])
    }) ;

   
  }

  ngOnDestroy(){

  }

  submit(){
    
    this.client = new Client(this.clientForm.value);
      
      this.clientService.createClient(this.client);

      this.clientForm.reset();
      this.dialogRef.close('Close');

      this.snackBar.open(this.client.lastName+" is added !","ADD Client",{ duration : 2000}); 
      
      //this.router.navigate(['/clients'])
  }

  onClose(){
    this.dialogRef.close('Close');
  }
}
