import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { ContratServices } from '../../contrat/contrat.service';
import { Clause } from '../../contrat/clause.modele';

@Component({
  selector: 'app-add-clause-dialog',
  templateUrl: './add-clause-dialog.component.html',
  styleUrls: ['./add-clause-dialog.component.css']
})
export class AddClauseDialogComponent implements OnInit {

  clauseForm : FormGroup;
  clause :Clause;

  constructor(
    private contractService : ContratServices,
    private snackBar: MatSnackBar,
    private dialogRef : MatDialogRef<AddClauseDialogComponent>,
    private translate : TranslateService    
         ) { }

  ngOnInit() {

    this.clauseForm = new FormGroup({
      "clauseTitle" : new FormControl (null, [Validators.required]),
      "clauseDesc"  : new FormControl (null, [Validators.required])
    });
  }

  submit(){
    this.clause = new Clause(this.clauseForm.value);
    this.contractService.createNewClause(this.contractService.currentContract.contractId, this.clauseForm.get("clauseTitle").value, this.clauseForm.get("clauseDesc").value)
            .subscribe(()=>{

                        },
            (error)=> {
              if(error.status == 200){
                this.clauseForm.reset();
                this.dialogRef.close("close");
                this.snackBar.open("New clause is added","ADD Clause",{ duration : 1000});
              }else{
                console.log("Error :",error)
              }
       });

  }



}
