import { Component, OnInit } from '@angular/core';
import { MatDialogRef ,MatSnackBar} from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Account } from '../../compte/compte.modele';
import { AccountService } from '../../compte/compte.service';
@Component({
  selector: 'app-add-account-dialog',
  templateUrl: './add-account-dialog.component.html',
  styleUrls: ['./add-account-dialog.component.css']
})
export class AddAccountDialogComponent implements OnInit {
  accountForm: FormGroup;
  account : Account;

  accountType  : string = "CLASSIC_ACCOUNT";
  accountTypes : string [] = ["CLASSIC_ACCOUNT", "LIVRET_A", "LONG_TERM_ACCOUNT"];

  constructor(private dialogRef : MatDialogRef<AddAccountDialogComponent>,
              private accountService : AccountService,
              private snackBar: MatSnackBar) {

  }

  ngOnInit() {

    this.accountForm = new FormGroup({
      'accountId'  : new FormControl(null,[Validators.required]),
      'clientId'   : new FormControl(null, [Validators.required]),
      'accountType': new FormControl(null, [Validators.required]),
      'balance'    : new FormControl(null,[Validators.required, Validators.pattern("^[0-9]{1,30}$")])
    }) ;
  }

  submit(){
    this.account  = new Account(this.accountForm.value);
     
    this.accountService.createNewAccount(this.account)
          .subscribe(
            ()=>{},
            (error)=>{
              if(error.status==200){
                this.accountForm.reset();
                this.dialogRef.close('Close');
                this.snackBar.open("New account is created!","New Account",{ duration : 2000});
              }else if (error.status==500){
                this.snackBar.open("You don't have the right contract!!!","Failed!!",{ duration : 3000});
              }else{
                this.snackBar.open("Check your api serve!!!","Failed!!",{ duration : 3000});
              }
            }
          );

  }

  onClose(){
    this.dialogRef.close('Close');
  }
}
