import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatButtonModule, MatInputModule, MatFormFieldModule, MatIconModule, MatSnackBarModule, MatSelectModule } from '@angular/material';
import { BrowserAnimationsModule }          from '@angular/platform-browser/animations';

import { AddClientDialogComponent }         from './add-client-dialog/add-client-dialog.component';
import { OperationAccountDialogComponent }  from './operation-account-dialog/operation-account-dialog.component';
import { FilterPipe } from './shared.pipe';
import { AddContractDialogComponent } from './add-contract-dialog/add-contract-dialog.component';
import { AddAccountDialogComponent } from './add-account-dialog/add-account-dialog.component';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AddClauseDialogComponent } from './add-clause-dialog/add-clause-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatSelectModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    })
  ],
  declarations: [
    AddClientDialogComponent,
    OperationAccountDialogComponent,
    FilterPipe,
    AddContractDialogComponent,
    AddAccountDialogComponent,
    AddClauseDialogComponent
  ],
  exports:[
    AddClientDialogComponent,
    OperationAccountDialogComponent,
    FilterPipe
  ]
})

export class SharedModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}