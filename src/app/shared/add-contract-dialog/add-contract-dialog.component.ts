import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ContratServices } from '../../contrat/contrat.service';
import { MatDialogRef, MatSnackBar } from '@angular/material';

import { Clause } from '../../contrat/clause.modele';
import {TranslateService} from '@ngx-translate/core';
import { Contract } from '../../contrat/contrat.modele';
import { Subscription } from 'rxjs/Subscription';


@Component({
  selector: 'app-add-contract-dialog',
  templateUrl: './add-contract-dialog.component.html',
  styleUrls: ['./add-contract-dialog.component.css']
})
export class AddContractDialogComponent implements OnInit, OnDestroy {

  contractForm: FormGroup;
  sub = new Subscription();
  contract : Contract;
  contractTypes : string[] = ["TERMS_AND_CONDITIONS", "ACCOUNT_CREATION", "BANK_LOAN"];

  constructor(private contractService : ContratServices,private snackBar: MatSnackBar,
              private dialogRef : MatDialogRef<AddContractDialogComponent>,
              private translate : TranslateService ) { }

  ngOnInit() {

    this.contractForm = new FormGroup({
      "contractId"  :  new FormControl(null,[Validators.required]),
      "contractType":  new FormControl(null,[Validators.required]),
      "description" :  new FormControl(null,[Validators.required])
    });
  }


  submit(){
    this.contract = new Contract(this.contractForm.value);

    this.sub = this.contractService.createContract(this.contract)
                .subscribe(
                  ()=> {},
                  (error)=>{
                    if(error.status == 200){
                      this.contractForm.reset();
                      this.dialogRef.close("close");
                      this.snackBar.open("Contract "+this.contract.contractId+" is added","ADD CONTRACT",{ duration : 1000});
                      this.contractService.newContract.next(true);
                    }else{
                      console.log("Check your api server!")
                    }
                  }
                );

  }

  ngOnDestroy(){
    this.sub.unsubscribe();
}

}
