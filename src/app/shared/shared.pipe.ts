import { Pipe, PipeTransform } from '@angular/core';
import { Client } from '../client/client.modele';

@Pipe({
  name: 'clientsearch',
  pure: false
})
export class FilterPipe implements PipeTransform {

  transform(clients: Client[],searchText: string, searchType:boolean): any {

    if(searchType){
      return clients.filter( client=>{
        return client.firstName.toLowerCase().includes(searchText.toLowerCase());
      })
    }else{
      return clients.filter( client=>{
        return client.lastName.toLowerCase().includes(searchText.toLowerCase());

      })      
    }
    
  }

}
