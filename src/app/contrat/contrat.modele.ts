import { Clause } from './clause.modele'

export class Contract{

    public contractId   : string;
    public contractType : string;
    public description  : string;
    public creationDate : Date;
    public clauses : Clause[];

    constructor( args : Contract){
        this.contractId     = args.contractId;
        this.contractType   = args.contractType;
        this.description    = args.description;
        this.creationDate   = args.creationDate;
        this.clauses        = [];
    }

}