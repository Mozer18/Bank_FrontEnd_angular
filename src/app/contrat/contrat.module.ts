import { NgModule } from '@angular/core'; 
import { CommonModule } from '@angular/common';

import { ContratComponent }         from './contrat.component';
import { ContratDetailComponent }   from './contrat-detail/contrat-detail.component';
import { ContratListComponent }     from './contrat-list/contrat-list.component';
import { ContratItemComponent }     from './contrat-list/contrat-item/contrat-item.component';

import { ContratServices }          from './contrat.service';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatButtonModule, MatInputModule, MatFormFieldModule, MatIconModule, MatOptionModule, MatSelectModule, MatTableModule, MatCardModule, MatExpansionModule, MatToolbar, MatToolbarModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddContractDialogComponent } from '../shared/add-contract-dialog/add-contract-dialog.component';

import { AuthGardService as AuthGuard} from '../auth-gard.service';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { ContractClausesComponent } from './contrat-list/contrat-item/contract-clauses/contract-clauses.component';
import { AddClauseDialogComponent } from '../shared/add-clause-dialog/add-clause-dialog.component';

const contratRoutes: Routes = [
    {path: 'contracts' , component: ContratComponent, canActivate: [AuthGuard],
    children:[
      {path: ':id' , component: ContratDetailComponent}
    ]},
    {path: 'contract-clauses', component: ContractClausesComponent, canActivate: [AuthGuard],
    children:[
        {path: ':id', component: ContractClausesComponent}
    ]}
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(contratRoutes),
        FormsModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatIconModule,
        MatOptionModule,
        MatSelectModule,
        MatTableModule,
        MatToolbarModule,
        MatCardModule,
        MatExpansionModule,
        BrowserAnimationsModule, 
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })       
    ],

    declarations: [
        ContratComponent,
        ContratDetailComponent,
        ContratListComponent,
        ContratItemComponent,
        ContractClausesComponent
      
    ],
    entryComponents:[AddContractDialogComponent, AddClauseDialogComponent],
    providers: [ 
        ContratServices
    ],

    exports: [ ContratComponent]
})
export class ContratModule{}

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}