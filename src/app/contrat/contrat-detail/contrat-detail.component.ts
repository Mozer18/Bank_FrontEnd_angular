import { Component, OnInit, Input, Output } from '@angular/core';
import { Contract } from '../contrat.modele';
import { ContratServices } from '../contrat.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Client } from '../../client/client.modele';
import { MatTableDataSource } from '@angular/material';
import { Clause } from '../clause.modele';


@Component({
  selector: 'app-contrat-detail',
  templateUrl: './contrat-detail.component.html',
  styleUrls: ['./contrat-detail.component.css']
})
export class ContratDetailComponent implements OnInit {
  
  client : Client;
  contract : Contract;

  constructor(private contractService : ContratServices,
              private route : ActivatedRoute) { }

  //dataSource = new MatTableDataSource<Clause>(this.clauses);
 
  ngOnInit() {
    this.route.params.subscribe((param: Params)=>{
      this.contract=new Contract(this.contractService.getSelectedContract(param["id"]));   
      console.log(this.contract); 
    });
  
  }

  onSingContract(contratId : string){
   // this.contractService.signContract(contratId , this.client.clientId);
  }

}
