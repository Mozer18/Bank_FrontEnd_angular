
export class Clause{

    public clauseTitle: string;
    public clauseDesc: string;

    constructor(args : Clause){
        this.clauseTitle = args.clauseTitle;
        this.clauseDesc  = args.clauseDesc;
    }

   
}