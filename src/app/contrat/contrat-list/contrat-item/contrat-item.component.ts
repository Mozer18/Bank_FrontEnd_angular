import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Contract } from '../../contrat.modele';
import { ContratServices } from '../../contrat.service';
import { Router } from '@angular/router';
import { ClientServices } from '../../../client/client.service';
import { Subscription } from 'rxjs/Subscription';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-contrat-item',
  templateUrl: './contrat-item.component.html',
  styleUrls: ['./contrat-item.component.css']
})
export class ContratItemComponent implements OnInit, OnDestroy {
  
  @Input() contract : Contract;
  sub : Subscription;

  constructor(private contractServices: ContratServices, 
              private router : Router,
              private snackBar: MatSnackBar,
              private clientService : ClientServices) { }

  ngOnInit() {

  }

  ngOnDestroy(){
   // this.sub.unsubscribe();
  }

  onViewClauses(contract: Contract){
    this.contractServices.currentContract= contract;
    this.router.navigate(["/contract-clauses/"+contract.contractId]);
  }

  onSignContract(contract: Contract){
    this.contractServices.signContract(contract.contractId, this.clientService.currentClient.clientId)
    .subscribe(
      ()=>{},
      (error)=>{
        if(error.status==200){
          this.snackBar.open("Contract signed!","Done",{ duration : 1000});
        }else{
          console.log("Check your api params!!!");
        }
      }
    );
  }

}
