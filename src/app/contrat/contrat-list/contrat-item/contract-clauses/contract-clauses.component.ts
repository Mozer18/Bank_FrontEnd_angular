import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import { Contract } from '../../../contrat.modele';
import { ContratServices } from '../../../contrat.service';
import { truncate } from 'fs';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AddClauseDialogComponent } from '../../../../shared/add-clause-dialog/add-clause-dialog.component';
import { Client } from '../../../../client/client.modele';
import { ClientServices } from '../../../../client/client.service';
@Component({
  selector: 'app-contract-clauses',
  templateUrl: './contract-clauses.component.html',
  styleUrls: ['./contract-clauses.component.css']
  //changeDetection : ChangeDetectionStrategy.OnPush
})
export class ContractClausesComponent implements OnInit {
  contract: Contract ;
  emptyTitle : boolean = false;
  noClauses : boolean = false; 

  dialogConfig = new MatDialogConfig();
  currentClient : Client;

  constructor(private contractService : ContratServices, 
              private addDialog : MatDialog,
              private clientService : ClientServices) { }

  ngOnInit() {
   this.start();
   this.currentClient = this.clientService.currentClient;
  }


  openDialog(){
    this.dialogConfig.width= "400px";
    this.addDialog.open(AddClauseDialogComponent,this.dialogConfig);
  }

  start(){
    this.contract = this.contractService.currentContract;
   
     if(this.contract.contractId==""){
      this.emptyTitle = true;
    } else{
      this.emptyTitle = false; 
    }  
     if(this.contract.clauses == null){
      this.noClauses = true;
    } else{
      this.noClauses = false; 
    }  
  }
}
