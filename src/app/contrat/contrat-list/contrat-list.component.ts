import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Contract } from '../contrat.modele';
import { ContratServices } from '../contrat.service';
import { empty } from 'rxjs/Observer';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { AddContractDialogComponent } from '../../shared/add-contract-dialog/add-contract-dialog.component';
import { Clause } from '../clause.modele';
import { Subscription } from 'rxjs/Subscription';
import { ClientServices } from '../../client/client.service';
import { Client } from '../../client/client.modele';

@Component({
  selector: 'app-contrat-list',
  templateUrl: './contrat-list.component.html',
  styleUrls: ['./contrat-list.component.css']
})
export class ContratListComponent implements OnInit, OnDestroy {

  sub : Subscription[]=[];
  dialogConfig = new MatDialogConfig();
  
  contracts : Contract[]=[];

  currentClient : Client;

  constructor(private contratServices : ContratServices, 
              private addDialog : MatDialog,
              private clientService : ClientServices) { 
    
  }

  ngOnInit() {
    this.initPage();
    this.currentClient = this.clientService.currentClient;
    this.sub[1] = this.contratServices.newContract.subscribe(
      ()=>{
        this.initPage();
        console.log(this.contracts);
      }
    );
    
  }

  initPage(){
    this.sub[0] = this.contratServices.getAllContracts().subscribe(
      (cts: Contract[]) => {
          this.contracts = cts;
      },
      (error:any)=>{console.log("Check your API server!")}
    );
  }

  ngOnDestroy(){
    this.sub.forEach(subscription => {
      subscription.unsubscribe();
    });
    
  }
  
  openDialog(){
    this.dialogConfig.width= "400px";
    this.addDialog.open(AddContractDialogComponent,this.dialogConfig);
  }
}
