import {EventEmitter, Injectable} from '@angular/core'
import { Contract } from './contrat.modele';
import 'rxjs/add/operator/map';
import { apiConfiguration } from '../apiConfiguration';
import { Clause } from './clause.modele';
import { Subject } from 'rxjs/Subject';
import { ClientServices } from '../client/client.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ContratServices{
    
    currentContract : Contract;    

    contracts : Contract[]=[];

    newContract = new Subject<boolean>();


    constructor( private http : HttpClient, 
                 private apiUrl : apiConfiguration
                ){}

    //Get all contracts            
    getAllContracts() : Observable<Contract[]>{

        return this.http.get<Contract[]>(this.apiUrl.ServerWithApiUrl+"getAllContracts");
        
    }
    
    //Create a contract
    createContract(contract: Contract) {
       return this.http.post<Contract>(this.apiUrl.ServerWithApiUrl+"createContract",contract);
    }

    // Create a new clause 
    createNewClause(contractId: string, clauseTitle: string, clauseDesc: string){
  
        return this.http.post(this.apiUrl.ServerWithApiUrl+"createClause",
        {
            "contractId"    : contractId,
            "clauseTitle"   : clauseTitle,
            "clauseDesc"    : clauseDesc                        
        },
        {responseType: "json"});
    }

    // Sign a contract 
    signContract(contractId : string, clientId : string){
        return this.http.post(this.apiUrl.ServerWithApiUrl+"signContract",
        {
            "contractId"    : contractId,
            "clientId"      : clientId            
        },
        {responseType: "json"});
    }

    getSelectedContract(contratId : string){

        this.contracts.  forEach((contract: Contract)=>{
            if(contract.contractId==contratId){
                this.currentContract = new Contract(contract);
            }
        })
        return this.currentContract;        
        
    }

    setCurrentContract(contractId : string){
        return this.http.get<Contract>(this.apiUrl.ServerWithApiUrl+"getContract", 
                               {params : new HttpParams().set("contractId", contractId),
                               responseType: 'json'});
                               
    }

}