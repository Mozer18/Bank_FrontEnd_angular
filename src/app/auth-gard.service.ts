import { AuthService } from './auth/auth.service'


import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';



@Injectable()
export class AuthGardService implements CanActivate{
 
    constructor(private authService: AuthService, private router: Router ){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) :Observable<boolean> | Promise<boolean> | boolean{
   
       if(!this.authService.loggedIn){
        this.router.navigate(["login"]);
        return false;
       }

       return true;
    }

}