
export class AccountOperation{
    public clientId : string;
    public compteId : string;
    public motif    : string;
    public montant  : number;

    constructor(clientId : string, compteId:string, motif: string, motant: number){

        this.clientId=clientId;
        this.compteId=compteId;
        this.motif   =motif;
        this.montant =motant;
    }
}