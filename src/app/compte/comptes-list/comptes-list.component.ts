import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { AccountService } from '../compte.service';
import { Account} from '../compte.modele';
import { MatDialogConfig, MatDialog, MatTableDataSource } from '@angular/material';
import { AddAccountDialogComponent } from '../../shared/add-account-dialog/add-account-dialog.component';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ClientServices } from '../../client/client.service';
import { Client } from '../../client/client.modele';
@Component({
  selector: 'app-comptes-list',
  templateUrl: './comptes-list.component.html',
  styleUrls: ['./comptes-list.component.css']
})
export class ComptesListComponent implements OnInit, OnDestroy{  
 
  dialogConfig = new MatDialogConfig();
  date : Date = new Date("1995-12-17");
  displayedColumns = ['accountId' , 'clientId' , 'balance'];

  accounts: Account[]=[];

  currentClient : Client;
  
  searchByAccountID : string = "";
  searchByClientID  : string = "";

  sub = new Subscription();

  constructor(private accountService : AccountService, 
              private dialog : MatDialog,
              private router : Router,
              private clientService : ClientServices) { }
  
  dataSource = new MatTableDataSource<Account>([]);

  ngOnInit() {
    this.intiPage();
      
     }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }

  intiPage(){
    this.currentClient = this.clientService.currentClient;

    if(this.currentClient.role!="admin"){
      this.getClientAccounts(this.currentClient);
    }else{
      this.getAllAccounts();
    }
    this.dataSource.filterPredicate = (data : Account , filter : string) => 
                    (data.accountId.includes(filter) != false || data.clientId.includes(filter) !=false);
  }

  //Get all accounts
  getAllAccounts(){
    this.sub = this.accountService.getAllAccounts().subscribe(
      (accounts: Account[]) =>
       { this.accounts = accounts;
         this.dataSource = new MatTableDataSource<Account>(this.accounts);
        }
    );

  }

  //Get client accounts
  getClientAccounts(client : Client){
    this.sub = this.accountService.getClientAccounts(client.clientId).subscribe(
      (accounts: Account[]) =>
       { this.accounts = accounts;
         this.dataSource = new MatTableDataSource<Account>(this.accounts);
        }
    );
  }

  search(filterValue: string){
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;       
  }

  openDetails(account: Account){
    this.accountService.setCurrentAccount(account);
    this.router.navigate(['/account-operations/', account.accountId]);
  }

  openAddDialog(){
    this.dialogConfig.width= "400px";
    this.dialog.open(AddAccountDialogComponent,this.dialogConfig); 
  }
}
