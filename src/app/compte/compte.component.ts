import { Component, OnInit, OnDestroy } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';
import { Account } from './compte.modele';

@Component({
  selector: 'app-compte',
  templateUrl: './compte.component.html',
  styleUrls: ['./compte.component.css']
})
export class CompteComponent implements OnInit, OnDestroy{

  constructor(private translate : TranslateService) { }
;
  ngOnInit() {

  }

  ngOnDestroy(){
    
  }
}
