export class Transaction{
    public date : Date; 
    public amount : number;  
    public message : string; 
    public type : string; 
    public account : string;
    
    constructor(date : Date, amount : number,  message: string, type: string, account : string){
        this.date = date; 
        this.amount = amount;
        this.message = message; 
        this.type = type; 
        this.account = account;
    }
}