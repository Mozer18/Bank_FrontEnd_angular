import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule, MatDialogModule, MatButtonModule, MatInputModule, MatFormFieldModule, MatIconModule, MatPaginatorModule, MatCardModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';

import { CompteComponent }          from './compte.component';
import { ComptesListComponent }     from './comptes-list/comptes-list.component';
import { CompteItemComponent }      from './comptes-list/compte-item/compte-item.component';

import { AccountService }            from './compte.service';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AuthGardService as AuthGuard} from '../auth-gard.service';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient} from '@angular/common/http';
import { ComptesOperationsComponent } from './comptes-operations/comptes-operations.component';
import { TransferDialogComponent } from './comptes-operations/transfer-dialog/transfer-dialog.component';

const compteRoutes : Routes=[
  {path : 'comptes' , component :CompteComponent, canActivate: [AuthGuard],
      children :[
        {path: ':compteId' , component: CompteComponent}
      ]},
  {path : 'account-operations', component: ComptesOperationsComponent, canActivate: [AuthGuard], 
      children :[
        {path: ':compteId' , component: ComptesOperationsComponent}
  ]}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(compteRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    MatIconModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  declarations: [
    CompteComponent,
    ComptesListComponent,
    CompteItemComponent,
    ComptesOperationsComponent,
    TransferDialogComponent

  ],
  providers: [
    AccountService
  ],
  exports: [

  ]
})
export class CompteModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}