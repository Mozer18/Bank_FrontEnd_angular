import { Component, OnInit, ViewChild, AfterViewInit, Inject } from '@angular/core';
import { AccountService } from '../compte.service';
import { Account } from '../compte.modele';
import { MatTableDataSource, MatDialog, MatDialogConfig, MatPaginator } from '@angular/material';

import { Transaction } from '../transaction.model'
import { TransferDialogComponent } from './transfer-dialog/transfer-dialog.component';
import { LOCAL_STORAGE, StorageService} from 'angular-webstorage-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comptes-operations',
  templateUrl: './comptes-operations.component.html',
  styleUrls: ['./comptes-operations.component.css'],
})
export class ComptesOperationsComponent implements OnInit, AfterViewInit{
  account : Account; 
  noAccountSelected : boolean = false; 

  dialogConfig = new MatDialogConfig();

  constructor(private accountService : AccountService, private dialog : MatDialog,private router : Router,
      @Inject(LOCAL_STORAGE) private storage: StorageService) { 
        
                dialog.afterAllClosed.subscribe(() => {
                  this.transactions = this.accountService.getCurrentAccount().transactions; 
                  this.dataSource = new MatTableDataSource<Transaction>(this.transactions);
              });
              }
  
  transactions : Transaction [] = this.accountService.getCurrentAccount().transactions;
  dataSource = new MatTableDataSource<Transaction>(this.transactions);
  displayedColumns = ['date', 'amount', 'message', 'type', 'account'];

  @ViewChild(MatPaginator) paginator: MatPaginator ;

  ngOnInit() {
    this.account = this.accountService.getCurrentAccount();
    if(this.account.accountId === ""){
      this.noAccountSelected = true;
      this.router.navigate(['/account-operations']);
    } else{
      this.noAccountSelected = false;
    }
  }

  openTransferDialog(){
    this.dialogConfig.width= "400px";
    this.dialog.open(TransferDialogComponent, this.dialogConfig); 
  }

  ngAfterViewInit(){
    this.dataSource.paginator = this.paginator; 
  }

}
