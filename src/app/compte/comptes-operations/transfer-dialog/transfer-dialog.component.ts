import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AccountService } from '../../compte.service';
import { Transaction } from '../../transaction.model';
import { Account} from '../../compte.modele';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-transfer-dialog',
  templateUrl: './transfer-dialog.component.html',
  styleUrls: ['./transfer-dialog.component.css'],
  animations: [
    trigger('listAnimation', [
      state('in', style({
        opacity: 1,
        transform: 'translateX(0px)'
      })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100px)'
        }),
        animate(300)
      ]),
      transition('* => void', [
        animate(300, style({
          transform: 'translateX(100px)',
          opacity: 0
        }))
      ])
    ])
  ]
})
export class TransferDialogComponent implements OnInit {
  transferForm: FormGroup;
  submit : boolean = false; 
  date : Date; 
  type : string = "Payment";
  amount : number; 
  message : string; 
  beneficiaryAccount : string; 
  
  transaction : Transaction; 
  account : Account;

  constructor(private dialogRef : MatDialogRef<TransferDialogComponent>,
              private accountService : AccountService) {
              }

  ngOnInit() {
    this.transferForm = new FormGroup({
      'date' : new FormControl(null,[Validators.required]),
      'amount' : new FormControl(null, [Validators.required,Validators.pattern("^[0-9]+(\.[0-9]{1,2})?$")]),
      'message' : new FormControl(null, [Validators.required, Validators.pattern("^[a-zA-Z_-]{1,50}$")]),
      'beneficiaryAccount' : new FormControl(null, [Validators.required])
    });

    this.account = this.accountService.getCurrentAccount();
  }

  onClose(){
    this.dialogRef.close('Close');
  }

  updateDate(value : Date){
    this.date = value; 
  }

  updateAmount(value : number){
    this.amount = value;
  }

  updateMessage(value : string){
    this.message = value;
  }

  updatebeneficiaryAccount(value: string){
    this.beneficiaryAccount = value; 
  }

  onSubmit(){
    this.transaction = new Transaction(this.date, this.amount, this.message, this.type, this.beneficiaryAccount);
    this.accountService.getCurrentAccount().transactions.push(this.transaction);
    console.log(this.accountService.getCurrentAccount().transactions);
    this.dialogRef.close('Close');
  }

}
