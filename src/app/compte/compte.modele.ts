import { Transaction } from "./transaction.model";

export class Account{
    public accountId    : string;
    public clientId     : string;
    public accountType  : string;
    public balance      : number;
    public transactions : Transaction[];

    constructor(args : Account){
        this.accountId      = args.accountId;
        this.clientId       = args.clientId;
        this.accountType    = args.accountType
        this.balance        = args.balance;
        this.transactions   = [];
    }
}