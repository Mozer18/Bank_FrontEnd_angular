import { Injectable } from "@angular/core";
import  "rxjs/add/operator/map";

import { apiConfiguration } from "../apiConfiguration";
import { Account } from "./compte.modele";
import { AccountOperation } from "./operation.modele";
import { Transaction } from "./transaction.model";
import { Subject } from "rxjs/Subject";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

@Injectable()
export class AccountService{
    currentAccount : Account ; 


    accounts : Account[]=[];
    transactions : Transaction[] = [];

    public $transactions2 = new Subject<Transaction[]>();

    constructor(private http : HttpClient , private apiUrl : apiConfiguration){

    }

    // Get all accounts
    getAllAccounts() : Observable<Account[]>{
        return this.http.get<Account[]>(this.apiUrl.ServerWithApiUrl+"getAllAccounts", {responseType: 'json'})     
    }

    //Get all accounts of a specific client
    getClientAccounts(clientId : string) : Observable<Account[]>{

        return this.http.get<Account[]>(this.apiUrl.ServerWithApiUrl+"getClientAccounts", 
            {params: new HttpParams().set("clientId", clientId),responseType: 'json'} )
    }

    createNewAccount(account : Account){
        return this.http.post<Account>(this.apiUrl.ServerWithApiUrl+"createAccount", account, {responseType: 'json'});
    }

    setCurrentAccount(account : Account){
        this.currentAccount = new Account(account); 
    }

    getCurrentAccount(){
        return this.currentAccount; 
    }

    addTransaction(date: Date, type: string, amount: number, message: string, account: string){
        this.transactions.push({date,type,amount,message, account}); 
        //this.$transactions2.next([{beneficiary, amount,message}]);
    }

    getTransactions(): Transaction[]{
        return this.transactions; 
    }

 
}