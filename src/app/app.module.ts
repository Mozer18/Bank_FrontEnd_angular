import { BrowserModule }        from '@angular/platform-browser';
import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpModule }           from '@angular/http';


import { AppComponent }             from './app.component';
import { HeaderComponent }          from './header/header.component';

import { ContratModule }            from './contrat/contrat.module';
import { ClientModule }             from './client/client.module';
import { CompteModule }             from './compte/compte.module';
import { SharedModule }             from './shared/shared.module';
import { AuthModule }               from './auth/auth.module';
import { BlockDetailsModule }       from './block-details/block-details.module';

import { apiConfiguration}          from './apiConfiguration';
import { MatIconModule, 
          MatCardModule, 
          MatMenuModule, 
          MatSidenavModule, 
          MatToolbarModule, 
          MatListModule, 
          MatButtonModule, 
          MatExpansionModule,
          MatDatepickerModule,
          MatTableModule,
          MatPaginatorModule} from '@angular/material';
          
import { ClickOutsideModule } from 'ng-click-outside';
import { AddAccountDialogComponent } from './shared/add-account-dialog/add-account-dialog.component';
import { HomePageComponent } from './home-page/home-page.component';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import {AuthService} from './auth/auth.service';
import { TransferDialogComponent } from './compte/comptes-operations/transfer-dialog/transfer-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { StorageServiceModule } from 'angular-webstorage-service';
import { PageNotFindComponent } from './page-not-find/page-not-find.component';
import { AuthGardService as AuthGuard} from './auth-gard.service';

//import { RequestInterceptor } from './requestInterceptor'




const appRoutes : Routes= [

  {
    path : "clients" , loadChildren : './client/client.module#ClientModule'
  }, {
    path : "contrats", loadChildren : './contrat/contrat.module#ContratModule' 
  },{
    path : "comptes" , loadChildren : './compte/compte.module#CompteModule'
  },{
    path : "login", loadChildren : './auth/auth.module#AuthModule'
  },{
    path : "" , component : HomePageComponent
  },{
    path: "block-details", loadChildren : './block-details/block-details.module#BlockDetailsModule' 
  },{
    path: "Page-Not-Found", component : PageNotFindComponent
  },{
    path: "**", redirectTo : 'Page-Not-Found'
  }
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomePageComponent,
    PageNotFindComponent
  ],

  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpModule,
    MatIconModule,
    MatMenuModule,
    MatCardModule,
    MatExpansionModule,
    FormsModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    ContratModule,
    ClientModule,
    CompteModule,
    BlockDetailsModule,
    MatTableModule,
    MatPaginatorModule,
    SharedModule,
    ClickOutsideModule,
    MatSidenavModule,
    MatToolbarModule,
    StorageServiceModule,
    MatListModule,
    MatButtonModule,
    HttpClientModule,
    AuthModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
    
  ],

  providers: [
    apiConfiguration, 
    AuthGuard,AuthService,
/*     {      
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    } */
  ],

  bootstrap: [AppComponent],

  entryComponents: [AddAccountDialogComponent, TransferDialogComponent]
})


export class AppModule { }

export function HttpLoaderFactory(httpClient: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(httpClient, './assets/i18n/', '.json');
}