import { Injectable } from '@angular/core';

@Injectable()
export class TokenStorage{

    constructor(){}

    signOut(){
        window.sessionStorage.removeItem("userToken");
        window.sessionStorage.clear();
    }

    public saveToken(token: string) {
        window.sessionStorage.removeItem("userToken");
        window.sessionStorage.setItem("userToken",  token);
      }
}