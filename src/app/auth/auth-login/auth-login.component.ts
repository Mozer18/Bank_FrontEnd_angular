import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import {TranslateService} from '@ngx-translate/core';
import { Router } from '@angular/router';
import { ClientServices } from '../../client/client.service';
import { Subscription } from 'rxjs/Subscription';
import { Client } from '../../client/client.modele';


@Component({
  selector: 'app-auth-login',
  templateUrl: './auth-login.component.html',
  styleUrls: ['./auth-login.component.css']
})
export class AuthLoginComponent implements OnInit ,OnDestroy{

  loginForm : FormGroup;
  loggedIn = this.authService.loggedInStatus.subscribe();
  sub : Subscription;
 
  constructor(private authService : AuthService, 
              private clientService: ClientServices,
              private translate : TranslateService, 
              private router : Router) { }

  ngOnInit() {
    this.loginForm =new FormGroup({
      "email" : new FormControl(null,[Validators.email,Validators.required]),
      "password" :new FormControl(null,[Validators.required])
    });
  }

  onLogin(){
   
   this.sub = this.clientService.getLogIn(this.loginForm.get("email").value, this.loginForm.get("password").value)
    .subscribe(
      (clt) => {
        this.authService.toggleLogin(true);
        console.log("yesss!!!", clt);
       // this.clientService.currentClient = new Client(clt);
        this.router.navigate(["../clients"]);},
        (error)=> {
          if(error.status== 200){
            this.authService.toggleLogin(true);
            this.clientService.email = this.loginForm.get("email").value;
            this.router.navigate(["../clients"]);
          }else{
            console.log("Email or password is incorrect!");
          }
        }
    );
    /* .toPromise().then(
     (clt) => {console.log(clt);}
    );.then(() => {
        if(!(this.client.password==this.loginForm.get("password").value))
        {
          console.log("error");
        }else{
          this.authService.toggleLogin(true);
          this.clientService.currentClient= this.client;
          this.router.navigate(["../clients"]);
        }
    }); */

  } 

  ngOnDestroy(){
    this.sub.unsubscribe();
  }
}
