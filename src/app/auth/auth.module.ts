import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthLoginComponent } from './auth-login/auth-login.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthService } from './auth.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatInputModule, MatFormFieldModule, MatIconModule, MatCardModule } from '@angular/material';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient} from '@angular/common/http';

const authRoutes : Routes = [
    {path : 'login' , component : AuthLoginComponent}
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(authRoutes),
      FormsModule,
      ReactiveFormsModule,
      MatButtonModule,
      MatInputModule,
      MatFormFieldModule,
      MatIconModule,
      MatCardModule,
      TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
    ],
    declarations: [
        AuthLoginComponent
    ],
    providers:[
        AuthService
    ],
    exports:[
        AuthLoginComponent
    ]
})
export class AuthModule{

}

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}