import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";


@Injectable()
export class AuthService {
    loggedInStatus = new Subject<boolean>();

    loggedIn = false;

    constructor(){}

    toggleLogin(value : boolean){
        this.loggedInStatus.next(value);
        this.loggedIn= value;
    }

}