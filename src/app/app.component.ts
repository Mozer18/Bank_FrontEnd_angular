import { Component, OnInit, LOCALE_ID} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { AuthService } from './auth/auth.service';
import { BlockService } from './block-details/block.service';
import { Subscription } from 'rxjs/Subscription';
import { BlockData } from './block-details/block.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [{provide: LOCALE_ID, useValue: 'en-UK' }]
})

export class AppComponent implements OnInit{ 
  

  userLanguage = 'en';
  title = 'app';

  constructor(private translate : TranslateService, 
              private authService : AuthService, private blockService: BlockService) { }

  ngOnInit(){
    this.translate.setDefaultLang(this.userLanguage)
    
  }

  
}
