import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';

import { BlockDetailsComponent } from './block-details.component'
import { MatTableModule, MatPaginatorModule } from '@angular/material';
import { BlockService } from './block.service';
import { AuthGardService as AuthGuard} from '../auth-gard.service';

import { Routes, RouterModule } from '@angular/router';

const blockRoutes : Routes =[
  {path: "block-details", component: BlockDetailsComponent,
  canActivate: [AuthGuard]}
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(blockRoutes),
    MatTableModule,
    MatPaginatorModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    })
  ],
  declarations: 
  [
    BlockDetailsComponent
  ],
  providers:[
    BlockService
  ],
  exports:[
    BlockDetailsComponent 
  ]
})


export class BlockDetailsModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}