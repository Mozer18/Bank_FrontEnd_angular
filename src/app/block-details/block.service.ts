import { Injectable } from "@angular/core";
import { BlockData } from "./block.model";
import { HttpClient } from "@angular/common/http";
import { apiConfiguration } from "../apiConfiguration";
import { Observable } from "rxjs/Observable";

@Injectable()
export class BlockService{

    constructor(private http : HttpClient , private apiUrl : apiConfiguration ){

    }

    getBlocks(): Observable<BlockData[]>{
        return this.http.get<BlockData[]>(this.apiUrl.ServerWithApiUrl+"blocks",{responseType: "json"});
    }

}