import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { BlockData } from './block.model'
import { BlockService } from './block.service';
import { Subscription } from 'rxjs/Subscription';


@Component({
  selector: 'app-block-details',
  templateUrl: './block-details.component.html',
  styleUrls: ['./block-details.component.css']
})
export class BlockDetailsComponent implements OnInit, AfterViewInit, OnDestroy {

   sub : Subscription;
  blocks : BlockData [] = [];
  dataSource = new MatTableDataSource<BlockData>([]);
  displayedColumns = ['blockNumber','prevHash',  'blockHash', 'dataHash'];

  constructor(private blockService : BlockService) {
    
   }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.sub = this.blockService.getBlocks().subscribe(
      (blocs: BlockData[]) =>
       {this.blocks = blocs;
        this.dataSource = new MatTableDataSource<BlockData>(this.blocks); 
        this.dataSource.paginator = this.paginator;
      }
    );
    
  }

  ngAfterViewInit(){
    this.dataSource.paginator = this.paginator; 

  }

  ngOnDestroy(){
    this.sub.unsubscribe();
    
  }
    

}
