import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import {TranslateService} from '@ngx-translate/core';
import { Client } from '../client/client.modele';
import { ClientServices } from '../client/client.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  showArrow : boolean = false; 
  clientstatus : boolean = false;
  loggedIn: boolean = false;
  currentClient : Client;

  constructor(private router : Router, 
              private authService : AuthService, 
              private translate : TranslateService,
              private clientService : ClientServices) { }

  ngOnInit() {
    this.authService.loggedInStatus.subscribe(data => this.loggedIn=data);
  }

  logout(){
    this.router.navigate(["/"]);
    this.authService.toggleLogin(false);
  }

  profile(){
    this.router.navigate(['client-profile']);
  }

  toggleArrow(){
    this.showArrow = !this.showArrow; 
  }

  onClickedOutside(){
    if(this.showArrow = true){
      this.showArrow = false; 
    }
  }

}
