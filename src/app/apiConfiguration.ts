import { Injectable } from '@angular/core';


export class apiConfiguration {
    public ApiIP: string = "http://localhost";
    public ApiPort: string = "8080";
    public Server: string = this.ApiIP+":"+this.ApiPort;
    public ApiUrl: string = "/api/";
    public ServerWithApiUrl =  this.ApiUrl;
}